#include <cstddef>
#include <cmath>
#include <stdexcept>
#include <iostream>

template <typename T = int, std::size_t N = 3>
class Vector
{
  T* vet = nullptr;
  std::size_t size;  //size of vector

  public:
    Vector();
    Vector(const Vector<T, N>&);
    ~Vector();

    size_t getSize() const { return size; }                                  //get size of vector

    //overloaded operators
    double operator !() const;                                               //get norm of a vector
    double operator &(const Vector<T, N>&) const;                            //get angle between vectors (radians)
    Vector<T, N>& operator =(const Vector<T, N>&);                           //assignment operator
    bool operator ==(const Vector<T, N>&) const;                             //comparison operator: equality
    bool operator !=(const Vector<T, N>&) const;                             //comparison operator: inequality
    T operator  [](const std::size_t) const;                                 //squared brackets operator
    T& operator [](const std::size_t);                                       //squared brackets assignment operator
    double operator *(const Vector<T, N>&) const;                            //scalar product
    Vector<T, N> operator *(const double) const;                             //product scalar per vector
    Vector<T, N> operator *=(const double);                                  //product scalar per vector and assignment
    Vector<T, N> operator +(const Vector<T, N>&) const;                      //sum
    Vector<T, N> operator +=(const Vector<T, N>&);                           //sum and assignment
    Vector<T, N> operator -() const;                                         //unary minus operator
    Vector<T, N> operator -(const Vector<T, N>&) const;                      //subtraction
    Vector<T, N> operator -=(const Vector<T, N>&);                           //subtraction and assignment
    Vector<T, N> operator /(const double);                                   //division per scalar
    Vector<T, N> operator /=(const double);                                  //division per scalar and assignment
    Vector<T, 3> operator ^(const Vector<T, 3>&) const;                      //cross product
    double operator |(const Vector<T, N>&) const;                            //distance

    template <typename U, std::size_t M>
    friend std::istream& operator >>(std::istream&, Vector<U, M>&);          //input operator
    template <typename U, std::size_t M>
    friend std::ostream& operator <<(std::ostream&, const Vector<U, M>&);    //output operator

    //utility functions
    Vector<double, N> normalize() const;                                     //calculate versor
};

template <typename T, std::size_t N>
Vector<T, N>::Vector()
{
  size = N;
  vet = new T[size];
  for (int i = 0; i < size; i++)
    vet[i] = 0;
}

template <typename T, std::size_t N>
Vector<T, N>::Vector(const Vector<T, N>& v)
{
  size = N;
  vet = new T[size];
  for (int i = 0; i < size; i++)
    vet[i] = v.vet[i];
}

template <typename T, std::size_t N>
Vector<T, N>& Vector<T, N>::operator =(const Vector<T, N>& v)
{
  for (int i = 0; i < size; i++)
    vet[i] = v[i];
  return *this;
}

template <typename T, std::size_t N>
T Vector<T, N>::operator [](const std::size_t index) const
{
  if (index >= size)
    throw std::out_of_range("Error: can't access out of range vector data");

  return vet[index];
}

template <typename T, std::size_t N>
T& Vector<T, N>::operator [](const size_t index)
{
  if (index >= size)
    throw std::out_of_range("Error: can't access out of range vector data");
  return vet[index];
}

template <typename T, std::size_t N>
double Vector<T, N>::operator !() const
{
  double res = 0;
  for (int i = 0; i < size; i++)
    res += vet[i]*vet[i];

  return sqrt(res);
}

template <typename T, std::size_t N>
bool Vector<T, N>::operator ==(const Vector<T, N>& v) const
{
  for (int i = 0; i < size; i++)
  {
    if (vet[i] != v.vet[i])
      return false;
  }

  return true;
}

template <typename T, std::size_t N>
bool Vector<T, N>::operator !=(const Vector<T, N>& v) const
{
  return !(*this == v);
}

template <typename T, std::size_t N>
double Vector<T, N>::operator *(const Vector<T, N>& v) const
{
  double res = 0;
  for (int i = 0; i < size; i++)
    res += vet[i] * v.vet[i];
  return res;
}

template <typename T, std::size_t N>
Vector<T, N> Vector<T, N>::operator *=(const double lambda)
{
  *this = *this * lambda;
  return *this;
}

template <typename T, std::size_t N>
Vector<T, N> Vector<T, N>::operator /(const double lambda)
{
  return (*this * (1 / lambda));
}

template <typename T, std::size_t N>
Vector<T, N> Vector<T, N>::operator /=(const double lambda)
{
  *this = *this / lambda;
  return *this;
}

template <typename T, std::size_t N>
Vector<T, N> Vector<T, N>::operator *(const double lambda) const
{
  Vector<T, N> v;
  for (int i = 0; i < size; i++)
    v.vet[i] = lambda * vet[i];

  return v;
}

template <typename T, std::size_t N>
double Vector<T, N>::operator &(const Vector<T, N>& v) const
{
  double res = (*this * v)*(1/(!(*this)*!v));
  return acos(res);
}

template <typename T, std::size_t N>
Vector<T, 3> Vector<T, N>::operator ^(const Vector<T, 3>& v) const
{
  Vector<T, 3> res;
  res[0] = vet[1]*v[2] - v[1]*vet[2];
  res[1] = v[0]*vet[2] - vet[0]*v[2];
  res[2] = vet[0]*v[1] - v[0]*vet[1];
  return res;
}

template <typename T, std::size_t N>
Vector<T, N> Vector<T, N>::operator +(const Vector<T, N>& v) const
{
  Vector<T, N>* t = new Vector<T, N>;
  t->size = size;
  for (int i = 0; i < size; i++)
    t->vet[i] = vet[i] + v.vet[i];

  Vector<T, N> res(*t);
  delete t;
  return res;
}

template <typename T, std::size_t N>
Vector<T, N> Vector<T, N>::operator +=(const Vector<T, N>& v)
{
  *this = *this + v;
  return *this;
}

template <typename T, std::size_t N>
Vector<T, N> Vector<T, N>::operator -() const
{
  Vector<T, N>* t = new Vector<T, N>;
  t->size = size;
  for (int i = 0; i < size; i++)
    t->vet[i] = -vet[i];

  Vector<T, N> res(*t);
  delete t;
  return res;
}

template <typename T, std::size_t N>
double Vector<T, N>::operator |(const Vector<T, N>& v) const
{
  return (!(*this - v));
}

template <typename T, std::size_t N>
Vector<T, N> Vector<T, N>::operator -(const Vector<T, N>& v) const
{
  Vector<T, N> res;
  res = *this + (-v);
  return res;
}

template <typename T, std::size_t N>
Vector<T, N> Vector<T, N>::operator -=(const Vector<T, N>& v)
{
  for (int i = 0; i < size; i++)
    vet[i] -= v[i];

  return *this;
}

template <typename T, std::size_t N>
Vector<double, N> Vector<T, N>::normalize() const
{
  Vector<double, N> versor;
  versor = *this * (1/!(*this));
  return versor;
}

template <typename T, std::size_t N>
std::istream& operator >>(std::istream& is, Vector<T, N>& v)
{
  v.size = N;
  if (v.vet)
    delete [] v.vet;

  v.vet = new T[v.size];
  for (int i = 0; i < v.size; i++)
    is >> v.vet[i];

  return is;
}

template <typename T, std::size_t N>
std::ostream& operator <<(std::ostream& os, const Vector<T, N>& v)
{
  os << '[';
  for (int i = 0; i < v.size; i++)
  {
    os << v.vet[i];
    if (i != v.size - 1)
      os << ", ";
  }

  os << ']';
  return os;
}

template <typename T, std::size_t N>
Vector<T, N>::~Vector()
{
  delete [] vet;
}
