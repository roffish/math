#include "goodmath.h"
using namespace std;
int main()
{
  cout << "Test cin" << endl;
  Vector<double, 5> v2;
  cin >> v2;

  cout << "Test cout" << endl;
  cout << v2 << endl;

  cout << "Test getSize" << endl;
  cout << v2.getSize() << endl;

  cout << "Test norm" << endl;
  cout << !v2 << endl;

  cout << "Test assignment" << endl;
  Vector<double, 5> v3;
  v3 = v2;
  cout << v3 << endl;

  cout << "Test squared brackets" << endl;
  cout << v3[0] << " " << v3[2] << endl;
  v3[2] = 10;
  cout << v3 << endl;

  cout << "Test equality operator 1: (prints false)" << endl;
  cout << (v3 == v2) << endl;
  cout << "Test inequality operator: (prints true)" << endl;
  cout << (v3 != v2) << endl;
  cout << "Test equality operator 3: (prints true)" << endl;
  v3 = v2;
  cout << (v3 == v2) << endl;

  cout << "Test scalar product" << endl;
  cout << v3*v2 << endl;

  cout << "Test product scalar per vector" << endl;
  cout << v3*2.3 << endl;

  cout << "Test unary minus operator" << endl;
  cout << -v3 << endl;

  cout << "Test sum" << endl;
  cout << v3+v2 << endl;

  cout << "Test subtraction" << endl;
  cout << v3 - v2 << endl;

  cout << "Test unit vector" << endl;
  cout << v3.normalize() << endl;
  cout << !(v3.normalize()) << endl;

  cout << "Test angle between vectors" << endl;
  Vector<double, 5> v; v[0] = 1; Vector<double, 5> v4; v4[1] = 1;
  cout << (v & v4) << endl;

  cout << "Test operator *=" << endl;
  cout << (v *= 2) << endl;

  cout << "Test operator +=" << endl;
  cout << (v += v4) << endl;

  cout << "Test operator /" << endl;
  cout << (v / 2) << endl;

  cout << "Test operator /=" << endl;
  cout << (v /= 2) << endl;

  cout << "Test distance" << endl;
  cout << (v | v4) << endl;

  cout << "Test cross product" << endl;
  Vector<int, 3> vet; vet[0] = 1; vet[1] = 2;
  Vector<int, 3> vet1; vet1[0] = 2; vet1[1] = 1;
  cout << (vet^vet1) << endl;

  cout << "Test out of range exception" << endl;
  try
  {
    double b = v[7];
  }
  catch (out_of_range& oor)
  {
    cerr << oor.what() << endl;
  }
}
