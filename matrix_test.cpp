#include "goodmath.h"
using namespace std;

int main()
{
  cout << "Test default constructor" << endl;
  Matrix<> m;
  cout << m << endl;

  cout << "Test squared brackets assignment" << endl;
  m[12] = 3;
  m[22] = 4;
  m[1] = 3;
  cout << m << endl;

  cout << "Test copy constructor" << endl;
  Matrix<> g(m);
  cout << g << endl;

  cout << "Test sum" << endl;
  cout << g + m << endl;

  cout << "Test opposite" << endl;
  cout << -g << endl;

  cout << "Test subtraction" << endl;
  cout << g - m << endl;

  cout << "Test equality operator" << endl;
  cout << (g == m) << endl;

  cout << "Test inequality operator" << endl;
  cout << (g != m) << endl;

  cout << "Test sum and =" << endl;
  cout << (g += m) << endl;

  cout << "Test subtraction and =" << endl;
  cout << (g -= m) << endl;

  cout << "Test exception" << endl;
  cout << g[44] << endl;
}
