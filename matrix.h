#include <cstddef>
#include <iostream>
#include <stdexcept>

template <typename T = int, std::size_t N = 3, std::size_t M = 3>
class Matrix
{
  T* mat = nullptr;
  std::size_t raws;
  std::size_t columns;

  public:
    Matrix();                                                                         //null matrix
    Matrix(const Matrix<T, N, M>&);                                                   //copy constructor

    T operator [](const std::size_t) const;                                           //squared brackets operator
    T& operator [](const std::size_t);                                                //squared brackets assignment operator
    Matrix<T, N, M>& operator =(const Matrix<T, N, M>&);                               //assignment operator
    bool operator ==(const Matrix<T, N, M>&) const;                                   //comparison operator: equality
    bool operator !=(const Matrix<T, N, M>&) const;                                   //comparison operator: inequality
    Matrix<T, N, M> operator +(const Matrix<T, N, M>&) const;                         //sum
    Matrix<T, N, M> operator +=(const Matrix<T, N, M>&);                              //sum and assignment
    Matrix<T, N, M> operator -(const Matrix<T, N, M>&) const;                         //subtraction
    Matrix<T, N, M> operator -=(const Matrix<T, N, M>&);                              //subtraction and assignment
    Matrix<T, N, M> operator -() const;                                               //unary minus
    ~Matrix();                                                                        //destructor

    template <typename U, std::size_t P, std::size_t Q>
    friend std::ostream& operator <<(std::ostream&, const Matrix<U, P, Q>&);          //output operator
};

template <typename T, std::size_t N, std::size_t M>
Matrix<T, N, M>::Matrix()
{
  raws = N;
  columns = M;
  mat = new T[N*M];
  for (int i = 0; i < N*M; i++)
    mat[i] = 0;
}

template <typename T, std::size_t N, std::size_t M>
Matrix<T, N, M>::Matrix(const Matrix<T, N, M>& matrix)
{
  raws = matrix.raws;
  columns = matrix.columns;
  mat = new T[raws * columns];
  for (int i = 0; i < raws * columns; i++)
    mat[i] = matrix.mat[i];
}

template <typename T, std::size_t N, std::size_t M>
T Matrix<T, N, M>::operator [](const std::size_t n) const
{
  if (n % 10 >= columns)
    throw std::invalid_argument("Invalid argument to access Matrix");

  std::size_t r = n / 10;
  std::size_t c = n % 10;
  return mat[r*columns + c];
}

template <typename T, std::size_t N, std::size_t M>
T& Matrix<T, N, M>::operator [](const std::size_t n)
{
  if (n % 10 > columns)
    throw std::invalid_argument("Invalid argument to access Matrix");

  std::size_t r = n / 10;
  std::size_t c = n % 10;
  return mat[r*columns + c];
}

template <typename T, std::size_t N, std::size_t M>
Matrix<T, N, M>& Matrix<T, N, M>::operator =(const Matrix<T, N, M>& matrix)
{
  for (int i = 0; i < raws * columns; i++)
    mat[i] = matrix.mat[i];
  return *this;
}

template <typename T, std::size_t N, std::size_t M>
bool Matrix<T, N, M>::operator ==(const Matrix<T, N, M>& matrix) const
{
  for (int i = 0; i < raws * columns; i++)
    if (mat[i] != matrix.mat[i])
      return false;
  return true;
}

template <typename T, std::size_t N, std::size_t M>
bool Matrix<T, N, M>::operator !=(const Matrix<T, N, M>& matrix) const
{
  return !(*this == matrix);
}

template <typename T, std::size_t N, std::size_t M>
Matrix<T, N, M> Matrix<T, N, M>::operator +(const Matrix<T, N, M>& matrix) const
{
  Matrix<T, N, M> res;
  for (int i = 0; i < raws * columns; i++)
    res.mat[i] = mat[i] + matrix.mat[i];

  return res;
}

template <typename T, std::size_t N, std::size_t M>
Matrix<T, N, M> Matrix<T, N, M>::operator +=(const Matrix<T, N, M>& matrix)
{
  *this = *this + matrix;
  return *this;
}

template <typename T, std::size_t N, std::size_t M>
Matrix<T, N, M> Matrix<T, N, M>::operator -=(const Matrix<T, N, M>& matrix)
{
  *this = *this - matrix;
  return *this;
}

template <typename T, std::size_t N, std::size_t M>
Matrix<T, N, M> Matrix<T, N, M>::operator -() const
{
  Matrix<T, N, M> res;
  for (int i = 0; i < raws * columns; i++)
    res.mat[i] = -mat[i];
  return res;
}

template <typename T, std::size_t N, std::size_t M>
Matrix<T, N, M> Matrix<T, N, M>::operator -(const Matrix<T, N, M>& matrix) const
{
  Matrix<T, N, M> res;
  res = *this + -matrix;
  return res;
}

template <typename T, std::size_t N, std::size_t M>
std::ostream& operator <<(std::ostream& os, const Matrix<T, N, M>& matrix)
{
  for (int i = 0; i < matrix.raws; i++)
  {
    os << '[';
    for (int j = 0; j < matrix.columns; j++)
    {
      os << matrix.mat[i*matrix.columns + j] << ' ';
      if (j == matrix.columns - 1)
	      os << "\b]\n";
    }
  }
  return os;
}

template <typename T, std::size_t N, std::size_t M>
Matrix<T, N, M>::~Matrix()
{
  delete mat;
}
